export class UserModel {

	id: number;
	company: any;
	company_id: number;
	company_logo: string;
	created_at: string;
	department: any;
	email: string
	is_active: boolean;
	name: string;
	role: any;
	token: string
	updated_at: string;
	username: string;

	constructor(public details: any) {
		for (var index in details) {
			this[index] = details[index];
		}
	}

	getWorkingHours() {
		if (this.department[0] && this.department[0]['work_hours']) {
			return this.department[0]['work_hours'];
		} else {
			return parseInt(this.getDataFromCompanyOptions("workhours"));
		}
	}

	getDataFromCompanyOptions(name) {
		var options = this.company.options;
		var found = null;

		options.forEach((item) => {
			if (item.name === name) {
				found = item.value;
			}
		});

		return found;
	}

	getWorkDays() {
		return this.getDataFromCompanyOptions("workdays");
	}

}