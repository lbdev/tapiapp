export class TaskModel {

   id: number;
   task_id: number;
   user_id: number;
   time: string;
   time_val: number;
   is_urgent: boolean;
   date_assigned: string;
   date_completed: string;
   complete: string;
   created_at: string;
   updated_at: string;
   is_extra: boolean;
   key: any;
   note: string;
   project: string;
   color: string;
   code: any;
   on_leave: boolean;
   details: any;

	constructor(public task: any) {
		for (var index in task) {
			this[index] = task[index];
		}
	}

}