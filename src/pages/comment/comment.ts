import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import * as moment from 'moment';

// Providers
import { TaskProvider } from '../../providers/task-provider';
import { Alerts } from '../../providers/alerts';

/*
  Generated class for the Comment page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html'
})
export class CommentPage {

  comments: any;
  textComment: string;
  currentDate: any;
  submitting: boolean = false;

  constructor(
  	private navParams: NavParams,
    private viewCtrl: ViewController,
    public taskProvider: TaskProvider,
    public alerts: Alerts
  ) {
  	this.comments = navParams.data.comment;
  	this.textComment = this.comments.comment || null;
  	this.currentDate = navParams.data.date;
  }

  ionViewDidLoad() {
  	console.log(this.comments);
  }

  /**
   * Closes the comment modal
   */
  close() {
  	this.viewCtrl.dismiss();
  }

  /**
   * Submits the comment to server
   */
  submitComment() {
  	this.submitting = true;
  	this.taskProvider.addComment(moment(this.currentDate), this.textComment).then(
  		success => {
  			this.submitting = false;
  			this.alerts.show('Awesome', 'Your comment has been added successfully.');
  			this.viewCtrl.dismiss();
  		},
  		err => {
  			this.submitting = false;
  			this.alerts.show('Uh oh!', 'Your comment wasn\'t submitted. Please check your internet connection and try again.');
  		}
  	);
  }

}
