import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Providers
import { UserProvider } from '../../providers/user-provider';
import { SettingsProvider } from '../../providers/settings-provider';
import { NotificationProvider } from '../../providers/notification-provider';
import { Alerts } from '../../providers/alerts';
import { Helper } from '../../providers/helper';

/*
  Generated class for the Settings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})

export class SettingsPage {

  is_push: boolean;

  email: any;
  fullname: any;

  current_pw: any;
  new_pw: any;
  confirm_new_pw: any;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,

  	public userProvider: UserProvider,
  	public settingsProvider: SettingsProvider,
  	public notificationProvider: NotificationProvider,
  	public alerts: Alerts,
  	public helper: Helper
  ) {}

  ionViewDidLoad() {
  	console.log(this.userProvider);
  	let user = this.userProvider.user;

  	// Push
  	this.is_push = localStorage.getItem('pushToken') ? true : false;

  	// Personal details
  	this.email = user.email;
  	this.fullname = user.name;

  	// Password
  	this.resetPasswordValues();
  }

  /**
   * Toggles Push status
   */
  togglePush() {
  	if (this.is_push) {
  		this.notificationProvider.init();
  		return;
  	}

  	this.notificationProvider.unregister();
  }

  /**
   * Updates the user's name
   */
  updateName() {
  	if ( this.validateName() ) {
  		this.alerts.showLoader();
  		this.settingsProvider.updateName(this.fullname).then(
  			response => {

  				if (response['status'] !== 200) {
  					this.alerts.hideLoader();
	  				this.alerts.show('Oops', response['message']);
	  				return;
	  			}

	  			this.userProvider.user.name = this.fullname;
	  			this.userProvider.create(this.userProvider.user).then(() => {
  					this.alerts.hideLoader();
	  				this.alerts.show('Done', 'Your details have been updated.');
	  			});
  			},
  			err => {
	  			this.helper.serverError();
	  		}
  		);
  	}

  }

  /**
   * Updates user password
   */
  updatePassword() {
  	if ( this.validatePassword() ) {
  		this.alerts.showLoader();
  		this.settingsProvider.updatePassword(this.current_pw, this.new_pw).then(
  			response => {
  				this.alerts.hideLoader();
  				if (response['status'] !== 200) {
	  				this.alerts.show('Oops', response['message']);
	  				return;
	  			}

	  			this.resetPasswordValues();
	  			this.alerts.show('You\'re secure', 'Your password has been updated successfully.');
  			},
  			err => {
	  			this.helper.serverError();
	  		}
  		);
  	}
  }

  /**
   * Validates the provided name
   */
  validateName() {
  	if ( this.fullname.length < 6 ) {
  		this.alerts.show('Too short', 'Your name must be at least six characters long. Please try again.');
  		return false;
  	}

  	return true;
  }

  /**
   * Validates the provided password
   */
  validatePassword() {

  	if ( this.current_pw.length === 0) {
  		this.alerts.show('Oops', 'You forgot enter your curent password. Let’s try again.');
  	}

  	else if ( this.new_pw.length < 8 ) {
  		this.alerts.show('Uh oh', 'For your safety, your new password should be at least 8 characters long. Please try again.');
  	}

  	else if ( this.new_pw !== this.confirm_new_pw ) {
  		this.alerts.show('Oops', 'Your passwords don\'t match. Slow down and try again.')
  	}

  	else {
  		return true;
  	}

  	return false;
  }


  /**
   * Sets password values to empty string
   */
  resetPasswordValues() {
  	this.current_pw = '';
	this.new_pw = '';
	this.confirm_new_pw = '';
  }

}
