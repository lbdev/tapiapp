import { Component } from '@angular/core';
import { NavController, PopoverController, ModalController, NavParams, Events, ItemSliding, MenuController } from 'ionic-angular';
import * as moment from 'moment';
import * as Chart from 'chart.js'

// Models
import { UserModel } from '../../models/user-model';

// Pages
import { PopoverPage } from '../weekly/popover';
import { TaskDetailsPage } from '../task-details/task-details';
import { CommentPage } from '../comment/comment';

// Providers
import { ErrorCatcher } from '../../providers/error-catcher';
import { Config } from '../../providers/config';
import { Helper } from '../../providers/helper';
import { Alerts } from '../../providers/alerts';
import { UserProvider } from '../../providers/user-provider';
import { EmployeeTaskProvider } from '../../providers/employee-task-provider';
import { NotificationProvider } from '../../providers/notification-provider';
import { ApprateProvider } from '../../providers/apprate-provider';

@Component({
  selector: 'page-employee-weekly',
  templateUrl: 'employee-weekly.html'
})

export class EmployeeWeeklyPage {

  user: any;

  filterVal = "az";
  filterText = "Alphabetical";

  prevDatePickerVal: string = "";
  datePickerVal: string = "";
  dateText: string = "";

  tasks: any;
  leave: any;

  bookedChart: any;
  bookedTime: string;

  barVal: string = "scaleY(0)";
  taskDone: string;

  dateIsPast: boolean;
  dateIsOnLeave: boolean = false;

  prevTaskStatus: any;

  itemHeight = 93;
  taskContainerHeight: any;

  constructor(
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    public menutCtrl: MenuController,
    public navParams: NavParams,
    public events: Events,

    public errorCatcher: ErrorCatcher,
    public config: Config,
    public helper: Helper,
    public alerts: Alerts,
    public taskProvider: EmployeeTaskProvider,
    public userProvider: UserProvider,
    public notificationProvider: NotificationProvider,
    public apprateProvider: ApprateProvider
  ) {
    // Initialize user
    let employee = this.navParams.data.employee;
    employee.company = this.userProvider.user.company; // inherit company options from the user
    this.user = new UserModel(this.navParams.data.employee);

    // Set user to taskProvider
    this.taskProvider.user = this.user;
    this.taskProvider.initDB();

    // Initialize everything
    this.initializeApp(); 
    
  }

  ionViewDidLoad() {
  }

  ionViewDidEnter() {
    this.taskContainerHeight = document.getElementById('taskContainer').clientHeight - 3;
  }

  ionViewWillUnload() {
    // Unsubscribe events
    this.events.unsubscribe('efilter:changed', null);
    this.events.unsubscribe('edatepicker:changed', null);
    this.events.unsubscribe('etasks:loaded', null);
  }

  initializeApp() {

    // Initialize datePickerVal and dateText and if the currently picked date is from past
    this.datePickerVal = this.resolveDate(moment().format(), 1);
    this.prevDatePickerVal = this.datePickerVal;
    this.dateText = moment(this.datePickerVal).format('ddd MMMM DD');
    this.dateIsPast = this.isPast();


    // Fetch daily task
    this.taskProvider.getDailyTask(moment(this.datePickerVal)).then(
      data => {
        this.tasks = data['tasks'];
        this.leave = data['leave'];
        this.events.publish('etasks:loaded', data);
      },

      err => {
        this.errorCatcher.handleError(err);
      }
    );

    // Initialize event listeners
    this.events.subscribe('efilter:changed', (filterEventData) => {
      this.filterVal = filterEventData;
      this.changeFilterText();
      this.applyFilter();
    });

    this.events.subscribe('edatepicker:changed', (datePickerEventData) => {
      this.datePickerVal = datePickerEventData;

      this.changeDateText();
      this.dateIsPast = this.isPast();

      this.taskProvider.getDailyTask(moment(this.datePickerVal)).then(
        data => {
          this.tasks = data['tasks'];
          this.leave = data['leave'];
          this.events.publish('etasks:loaded', data);
        },

        err => {
          this.errorCatcher.handleError(err);
        }
      );
    });

    this.events.subscribe('etasks:loaded', (taskEventData) => {
      console.log(taskEventData);

      this.applyFilter();
      this.renderBookedGraph();
      this.renderDoneGraph();
    });
  }

  /**
   * Shows the filter popover
   * @param {Event} ev tap event
   */
  presentFilter(ev) {
    let popover = this.popoverCtrl.create(PopoverPage, {
      weeklycmp: this
    });

    popover.present({
      ev: ev
    });

    return;
  }

  /**
   * Triggers when the date picker value is changed
   */
  dateChanged() {
    this.datePickerVal = this.resolveDate(this.datePickerVal, moment(this.datePickerVal).diff(moment(this.prevDatePickerVal), 'days'));
    this.prevDatePickerVal = this.datePickerVal;
    this.events.publish('edatepicker:changed', this.datePickerVal);
  }

  /**
   * Resolves the correct date
   * This is useful when the work days have rest days
   * Skips the rest day when being selected in the date picker
   * @param {any}    intent_date intent date
   * @param {Number} direction   (1 = Forward, -1 = Backward)
   */
  resolveDate(intent_date, direction?) {
    direction = direction ? direction : 1;
    direction = direction < 0 ? -1 : 1;

    var resolve_date = moment(intent_date);
    var work_days = JSON.parse(this.user.getWorkDays());

    while (!this.helper.in_array(resolve_date.format('ddd'), work_days)) {
      resolve_date = resolve_date.add(direction, 'days');
    }

    return resolve_date.format();
  }

  /**
   * Changes filter text based on the selected filter
   */
  changeFilterText() {
    if (this.filterVal == "az") {
      this.filterText = "Alphabetical";
    } else if (this.filterVal == "pr") {
      this.filterText = "Priority";
    } else if (this.filterVal == "dl") {
      this.filterText = "Deadline"
    }

    return;
  }

  /**
   * Applies filter to task list
   */
  applyFilter() {

    this.tasks.sort(function(obj1, obj2) {
      return obj1.order - obj2.order;
    });

    // console.log(this.filterVal);
    // if (this.filterVal === "az") {
    //   this.tasks.sort(function(obj1, obj2) {
    //     if (obj1.details.task.key.name === obj2.details.task.key.name) return 0;
    //     if (obj1.details.task.key.name > obj2.details.task.key.name) return 1;
    //     if (obj1.details.task.key.name < obj2.details.task.key.name) return -1;
    //   });
    // }

    // else if (this.filterVal === "pr") {
    //   this.tasks.sort(function(obj1, obj2) {
    //     if (obj1.is_urgent === obj2.is_urgent) {
    //       if (obj1.details.task.key.name === obj2.details.task.key.name) return 0;
    //       if (obj1.details.task.key.name > obj2.details.task.key.name) return 1;
    //       if (obj1.details.task.key.name < obj2.details.task.key.name) return -1;
    //     }
    //     return obj2.is_urgent - obj1.is_urgent;
    //   });
    // }

    // else if (this.filterVal === "dl") {
    //   this.tasks.sort(function(obj1, obj2) {

    //     var d1 = moment(obj1.deadline, "DD-MM-YYYY");
    //     var d2 = moment(obj2.deadline, "DD-MM-YYYY");

    //     if (d1.diff(d2, "days") === 0) {
    //       if (obj1.details.task.key.name === obj2.details.task.key.name) return 0;
    //       if (obj1.details.task.key.name > obj2.details.task.key.name) return 1;
    //       if (obj1.details.task.key.name < obj2.details.task.key.name) return -1;
    //     }
    //     else if (d1.diff(d2, "days") > 0) {
    //       return 1;
    //     }

    //     return -1;

    //   });
    // }

  }

  /**
   * Changes and formats the selected date
   */
  changeDateText() {
    var date = moment(this.datePickerVal);
    this.dateText = date.format('ddd MMMM DD');

    return;
  }

  /**
   * Refreshes the task from pull refresh
   * @param {any} refresher refresher DOM
   */
  refreshTask(refresher) {

    this.taskProvider.refreshDailyTasks(moment(this.datePickerVal)).then(
      data => {
        this.tasks = data['tasks'];
        this.leave = data['leave'];
        this.events.publish('etasks:loaded', data);
        refresher.complete();
      },

      err => {
        refresher.complete();
        this.errorCatcher.handleError(err);
      }
    );
  }

  /**
   * Navigates the date forward
   */
  addDate() {
    var work_days = JSON.parse(this.user.getWorkDays());
    var intent_date = moment(this.datePickerVal);
    intent_date = intent_date.add(1, 'days');

    while (!this.helper.in_array(intent_date.format('ddd'), work_days)) {
      intent_date = intent_date.add(1, 'days');
    }

    var date = intent_date.format();
    this.datePickerVal = date;
    this.events.publish('edatepicker:changed', this.datePickerVal);

    return;
  }

  /**
   * Navigates the date backward
   */
  subtractDate() {
    var work_days = JSON.parse(this.user.getWorkDays());
    var intent_date = moment(this.datePickerVal);
    intent_date = intent_date.subtract(1, 'days');

    while (!this.helper.in_array(intent_date.format('ddd'), work_days)) {
      intent_date = intent_date.subtract(1, 'days');
    }

    var date = intent_date.format();
    this.datePickerVal = date;
    this.events.publish('edatepicker:changed', this.datePickerVal);

    return;
  }

  /**
   * Gets background from hex and returns in RGBA format
   * @param {any} hex [description]
   */
  getBg(hex: any) {
    var rgb = this.helper.hexToRgb(hex);

    return 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', 0.13)';
  }

  /**
   * Renders company logo
   * @param {any} companyLogo company logo file name
   */
  renderLogo(companyLogo: any) {
    return companyLogo ? 'url(' + this.config.getLogoFolderURL() + companyLogo + ')' : 'url(assets/images/logo-placeholder.png)';
  }

  /**
   * Toggle task status from circle button
   * @param {any} task task object
   */
  toggleFromCircle(task) {
    this.prevTaskStatus = task["complete"];
    task["complete"] = this.prevTaskStatus === "" ? "complete" : "";
    this.renderDoneGraph();
    this.toggleStatus(task);
  }

  /**
   * Toggles task status from sliding options
   * @param {ItemSliding} itemSliding Item sliding
   * @param {any}         task        task object
   */
  toggleStatusFromSliding(itemSliding: ItemSliding, task) {
    this.prevTaskStatus = task["complete"];
    task["complete"] = this.prevTaskStatus === "" ? "complete" : "";
    itemSliding.close();
    this.renderDoneGraph();
    this.toggleStatus(task);
  }

  /**
   * Toggles task status
   * @param {any} task task object
   */
  toggleStatus(task) {
    this.taskProvider.toggleTaskStatus(task.id, task["complete"]).then(
      data => {
        this.events.publish('taskstatus:toggled');
      },
      err => {
        this.errorCatcher.handleError(err);
        task["complete"] = this.prevTaskStatus;
        this.renderDoneGraph();
      }
    );
  }

  /**
   * Shows the task details modal
   * @param {Event}   event tap event
   * @param {any}     task  task object
   */
  presentTaskDetails(event, task) {
    var p = this.modalCtrl.create(TaskDetailsPage, {
      task: task
    });
    p.present();
  }

  /**
   * Renders the Time Booked graph
   */
  renderBookedGraph() {

    var FULL_WORKING_HRS = this.user.getWorkingHours();
    var total_hrs = 0;

    this.tasks.forEach(item => {
      total_hrs += item['time_val'];
    });

    if ( this.leave ) {
      if (this.leave['leave_type'] !== null) {
        total_hrs += this.leave['leave_hours'];
      }
    }

    var pie = document.getElementById('bookedgraph');
    var value = total_hrs / FULL_WORKING_HRS;
    var text = Math.round(value * FULL_WORKING_HRS);
    var isPast = this.isPast() || this.isLeaveFullDay();

    if (value > 1) {
      value = 1;
    }


    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [value, (1 - value)],
          backgroundColor: [isPast ? '#777' : 'rgb(0, 157, 156)', isPast ? 'rgba(0, 0, 0, 0)' : '#fff'],
          borderWidth: 0,
        }],
      },
      options: {
        responsive: true,
        tooltips: [{
          enabled: false
        }],
        hover: [{
          onHover: null
        }],

        cutoutPercentage: -1
      }
    };

    if (pie) {
      this.bookedChart = new Chart(pie, config);
      this.bookedTime = text + "hr";
    }
  }

  /**
   * Renders the Task Done graph
   */
  renderDoneGraph() {
    var total_tasks = 0;
    var total_completed = 0;

    this.tasks.forEach(item => {
      total_tasks++;

      if (item['complete'] === "complete") {
        total_completed++;
      }
    });

    total_tasks = total_tasks === 0 ? 1 : total_tasks;
    var completed_tasks_ratio = total_completed / total_tasks;

    this.barVal = "scaleY(" + completed_tasks_ratio + ")";
    this.taskDone = Math.round(completed_tasks_ratio * 100) + "%";
  }

  /**
   * Gets the value of date picker and checks if it is past
   * @returns {Boolean} returns true if its past, otherwise false
   */
  isPast() {
    var now = moment();
    var against_date = moment(this.datePickerVal);
    return against_date.diff(now, 'days') < 0 ? true : false;
  }

  /**
   * Jumps to the current date
   */
  jumpToToday() {
    var now = this.resolveDate(moment().format(), 1);

    console.log(moment(now).isSame(this.datePickerVal));

    if ( !moment(now).isSame(this.datePickerVal) ) {
      this.datePickerVal = now;
      this.events.publish('edatepicker:changed', this.datePickerVal);
    }
    
  }

  /**
   * Debugger for pull refresh (while pulling)
   * @param {Event} event     pull event
   * @param {any}   refresher refresher DOM
   */
  doPulling(event, refresher) {
    console.log(event);
    console.log(refresher.clientHeight);
  }

  /**
   * Handles swipe event, navigates the date
   * @param {any} event Event object
   */
  swipeEvent(event) {
    if ( event.direction === 2 ) {
      this.addDate()
    } else if ( event.direction === 4 ) {
      this.subtractDate();
    }
  }

  /**
   * Sets the task height based on hours allotted
   * @param {any} task Task object
   * @returns {number} height
   */
  setTaskHeight(task: any) {
    var FULL_WORKING_HRS = this.user.getWorkingHours();
    // var size = task['time_val'] <= 1 ? (1 * this.itemHeight) : task['time_val'] * this.itemHeight;
    var size = task['time_val'] <= 1 ? (1 * this.itemHeight) : (task['time_val'] / FULL_WORKING_HRS) * this.computeTaskContainerHeight();

    return size;
  }

  /**
   * Computes the task container's height
   * @returns {number} height
   */
  computeTaskContainerHeight() {
    return this.tasks.length > 1 ? this.taskContainerHeight - 10 : this.taskContainerHeight;
  }

  /**
   * Checks if its a full-day leave
   */
  isLeaveFullDay() {
    var FULL_WORKING_HRS = this.user.getWorkingHours();

    if ( !this.leave ) {
      return false;
    }

    if ( this.leave['leave_type'] !== null && (this.leave['leave_hours'] === FULL_WORKING_HRS || this.leave['leave_hours'] === 0 ) ) {
      return true;
    }

    return false;
  }


  /**
   * Checks if leave bar should appear
   */
  shouldShowLeaveBar() {
    var FULL_WORKING_HRS = this.user.getWorkingHours();

    if ( !this.leave ) {
      return false;
    }

    if ( this.leave['leave_type'] !== null && this.leave['leave_hours'] !== FULL_WORKING_HRS && this.leave['leave_hours'] !== 0 ) {
      return true;
    }

    return false;
  }

  /**
   * Renders leave text
   */
  renderLeaveText() {
    var text = '';
    
    if ( !this.leave ) {
      return text;
    }

    if ( this.leave['leave_type'] !== null ) {
      switch (this.leave['leave_type']) {
          case "annual":
              text = "Annual Leave";
              break;
          case "sick":
              text = "Sick Leave";
              break;
          case "maternal":
              text = "Maternal Leave";
              break;
          case "holiday":
              text = "Holiday";
              break;
      }
    }

    return text;
  }

  jumpToTodayAndRefreshTasks() {
    this.jumpToToday();
    this.alerts.showLoader();
    this.taskProvider.refreshDailyTasks(moment(this.datePickerVal)).then(
      data => {
        this.tasks = data['tasks'];
        this.leave = data['leave'];
        this.events.publish('etasks:loaded', data);
        this.alerts.hideLoader();
      },

      err => {
        this.alerts.hideLoader();
        this.errorCatcher.handleError(err);
      }
    );
  }


}







