import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Pages
import { EmployeeWeeklyPage } from '../employee-weekly/employee-weekly';

// Providers
import { UserProvider } from '../../providers/user-provider';
import { EmployeeProvider } from '../../providers/employee-provider';

@Component({
  selector: 'page-employee-list',
  templateUrl: 'employee-list.html'
})
export class EmployeeListPage {

  departments: any;
  queryText: string = '';
  user_id: number;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	private employeeProvider: EmployeeProvider,
  	private userProvider: UserProvider
  ) {
  	this.user_id = userProvider.user.id;
  }

  ionViewDidLoad() {
  	this.employeeProvider.loadEmployees().then(data => {
  		if ( Object.keys(data).length > 0 ) {
  			this.updateEmployee();
  		}
  	});
  }

  hasEmployees(dept: any) {

  	let toShow = false;

  	if ( Object.keys(dept.users).length > 0 ) {
  		dept.users.forEach((user: any) => {
  			if ( !user.hidden ) {
  				toShow = true;
  			}
  		});
  	}

  	return toShow;
  }

  updateEmployee() {
  	this.departments = this.employeeProvider.filterEmployees(this.queryText);
  }

  isMe(employee: any) {
  	console.log(employee);
  	return employee.id === this.user_id;
  }

  viewSchedule(employee: any) {
  	this.navCtrl.push(EmployeeWeeklyPage, {
      employee: employee
    });
  }

}
