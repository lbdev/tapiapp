import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

/*
  Generated class for the TaskDetails page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-task-details',
  templateUrl: 'task-details.html'
})
export class TaskDetailsPage {

  task: any;

  constructor(
  	private navParams: NavParams,
    private viewCtrl: ViewController) {

  	this.task = this.navParams.data.task;
  	console.log(this.task);

  }

  /**
   * Converts the text to human readable time
   * @param {Number} time task time
   */
  humanReadableTime(time): string {
  	var hr:number = time / 60;
    var min:number = time % 60;

    var text = "";

    if (Math.floor(hr) !== 0) {
      text += hr + (hr > 1 ? " hours" : " hour");
    }

    if (min !== 0) {
      text += " " + min + (min > 1 ? " minutes" : " minute");
    }

    return text;
  }

  /**
   * Closes the modal
   */
  close() {
  	this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
  }

}
