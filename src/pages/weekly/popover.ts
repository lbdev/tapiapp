import { Component } from '@angular/core';
import { ViewController, NavParams, Events } from 'ionic-angular';


@Component({
  template: `<ion-list radio-group [(ngModel)]="filterVal" class="popover-page" (ionChange)="changeFilter()">
      <ion-list-header>
        Filter
      </ion-list-header>
      <ion-item class="">
        <ion-label>Alphabetical</ion-label>
        <ion-radio value="az"></ion-radio>
      </ion-item>
      <ion-item class="">
        <ion-label>Priority</ion-label>
        <ion-radio value="pr"></ion-radio>
      </ion-item>
      <ion-item class="">
        <ion-label>Deadline</ion-label>
        <ion-radio value="dl"></ion-radio>
      </ion-item>
    </ion-list>`
})

export class PopoverPage {

  filterVal;

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private events: Events
  ) {
    if (this.navParams.data) {
      this.filterVal = this.navParams.data.weeklycmp.filterVal;
    }
  }

  changeFilter() {
    this.events.publish('filter:changed', this.filterVal);
  }
}