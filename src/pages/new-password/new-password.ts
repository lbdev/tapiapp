import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

// Providers
import { Alerts } from '../../providers/alerts';
import { Helper } from '../../providers/helper';
import { ErrorCatcher } from '../../providers/error-catcher';
import { Auth } from '../../providers/auth';
import { PasswordProvider } from '../../providers/password-provider';
import { UserProvider } from '../../providers/user-provider';

// Pages
import { WeeklyPage } from '../weekly/weekly';

/*
  Generated class for the NewPassword page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-new-password',
  templateUrl: 'new-password.html'
})
export class NewPasswordPage {

  pwForm: FormGroup;
  hasSubmitted: boolean = false;

  email: any;
  password: any;

  userData: any;

  bg: any;

  constructor(
  	public navCtrl: NavController,
  	private formBuilder: FormBuilder,
  	private navParams: NavParams,
    public errorCatcher: ErrorCatcher,
  	public alerts: Alerts,
  	public helper: Helper,
  	public auth: Auth,
  	public passwordProvider: PasswordProvider,
  	public userProvider: UserProvider
  ) {

  	// Set up form
  	this.pwForm = formBuilder.group({
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        password_confirm: ['', Validators.compose([Validators.required])],
    });

    // Get nav params
    console.log(this.navParams);
    this.email = this.navParams.data.email;
    this.password = this.navParams.data.password;
    this.userData = this.navParams.data.userData;
    this.bg = this.navParams.data.bg;
  }

  ionViewDidLoad() {
  }

  /**
   * Performs password reset request to server
   */
  updatePW() {
  	if ( this.validate() ) {

  		var formdata = {};
  		formdata['email'] = this.email;
  		formdata['password'] = this.pwForm.value['password'];
  		formdata['password_confirm'] = this.pwForm.value['password_confirm'];
  		formdata['temp_password'] = this.password;

  		this.hasSubmitted = true;
  		this.passwordProvider.updatePassword(formdata).then( 
  			isPasswordUpdated => {
  				this.hasSubmitted = false;

  				// Make a login using the updated credentials
  				var credentials = {};
  				credentials['email'] = this.email;
  				credentials['password'] = this.pwForm.value['password'];

  				this.alerts.showLoader();
		  		this.auth.login(credentials).then(
		  			data => {
			  			this.alerts.hideLoader();

			  			if (data['status'] !== 200) {
			  				this.alerts.show('Oops', data['message']);
			  				return;
			  			}

              // if ( data['data']['role'][0]['slug'] == "employee" ) 

			        if ( true) {
			        	this.userProvider.create(data['data']).then(
			              success => {
			                this.navCtrl.push(WeeklyPage, {
			                  fromLogin: true
			                });
			              }
			            );

			          } 

			        // else {
			        //     this.alerts.show('Warning', 'This app is for employees only, please use the Tapi website for admins and scheduler.');
	          //   }
			  			
			  		}, 
			  		err => {
			  			// this.alerts.hideLoader();
			  			this.helper.serverError();
			  		}
		  		);
  			},
  			err => {
  				this.hasSubmitted = false;
  				this.errorCatcher.handleError(err);
  			}
  		);
  	}
  	
  }

  /**
   * Validates the form
   */
  validate() {
  	let input = this.pwForm.value;

  	if ( input['password'].length < 8 ) {
  		this.alerts.show('Uh oh', 'For your safety, your password should be at least 8 characters long. Please try again.');
  	}

  	else if ( input['password_confirm'].length === 0 ) {
  		this.alerts.show('Oops', 'You forgot confirm your password. Let’s try again.')
  	}

  	else if ( input['password'] !== input['password_confirm'] ) {
  		this.alerts.show('Oops', 'Your passwords don\'t match. Slow down and try again.')
  	}

  	else {
  		return true;
  	}

  	return false;

  }

}
