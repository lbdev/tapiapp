import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

// Providers
import { Alerts } from '../../providers/alerts';
import { ErrorCatcher } from '../../providers/error-catcher';
import { Helper } from '../../providers/helper';
import { PasswordProvider } from '../../providers/password-provider';

// Validators
import { EmailValidator } from '../../validators/email';

/*
  Generated class for the ForgotPassword page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {

  fpform : FormGroup;
  hasSubmitted: boolean = false;
  bg: any;

  constructor(
  	public navCtrl: NavController,
  	private formBuilder: FormBuilder,
  	public alerts: Alerts,
  	public helper: Helper,
  	public passwordProvider: PasswordProvider,
    public params: NavParams,
    public errorCatcher: ErrorCatcher
  ) {

    // Set bg
    this.bg = this.params.get('bg');

    // Set up form builder
  	this.fpform = formBuilder.group({
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
    });
  }

  ionViewDidLoad() {
  }

  /**
   * Submits the forgot password request to server
   */
  sendForgotPW() {
  	if (this.validate() && !this.hasSubmitted) {
  		this.hasSubmitted = true;
  		this.passwordProvider.requestForgotPW(this.fpform.value).then(
  			hasRequested => {
  				this.hasSubmitted = false;
  				this.alerts.show('It happens to the best of us', 'We have sent you instructions on how to reset your password. Please check your email.');
          this.navCtrl.pop();
  			},
  			err => {
  				this.hasSubmitted = false;
  				this.errorCatcher.handleError(err);
  			}
  		);
  	}
  }

  /**
   * Validates the form
   */
  validate() {
  	let formControl = this.fpform.controls;

  	if ( !formControl['email'].valid ) {
  		this.alerts.show('Oops', 'Your email address format is not correct. Please try again.');
  		return false;
  	}

  	return true;
  }

}
