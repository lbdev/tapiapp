import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';

// Pages
import { WeeklyPage } from '../weekly/weekly';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { NewPasswordPage } from '../new-password/new-password';

// Providers
import { Alerts } from '../../providers/alerts';
import { Auth } from '../../providers/auth';
import { Helper } from '../../providers/helper';
import { UserProvider } from '../../providers/user-provider';

// Validators
import { EmailValidator } from '../../validators/email';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  loginForm : FormGroup;

  submitAttempt = false;

  bgs = ['greens', 'blues', 'reds'];
  generatedNum: number;

  refWindow: any;


  constructor(
  	public navCtrl: NavController,
  	private formBuilder: FormBuilder,
  	public alerts: Alerts,
  	public auth: Auth,
  	public helper: Helper,
  	public storage: Storage,
  	public userProvider: UserProvider
  ) {

  	this.loginForm = formBuilder.group({
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        password: ['', Validators.compose([Validators.required])]
    });

  }

  ionViewDidLoad() {
    
  }

  ionViewDidEnter() {
    this.adjustFormMargin();
  }

  ionViewWillEnter() {
    this.generatedNum = this.helper.getRandomNumber(0, 2);
  }

  adjustFormMargin() {
    let loginForm: any = document.getElementById('loginForm');
    let windowHeight: number = window.innerHeight;
    let formHeight: number = loginForm.clientHeight;
    let intentMargin: number = Math.abs(( windowHeight / 2 ) - ( formHeight / 2 ));

    loginForm.style.marginTop = intentMargin + "px";
  }

  /**
   * Performs login request to server
   */
  login() {
    var me = this;

  	if ( this.validate() ) {
  		this.alerts.showLoader();
  		this.auth.login(this.loginForm.value).then(
  			data => {
	  			this.alerts.hideLoader();

	  			if (data['status'] !== 200) {
	  				this.alerts.show('Oops', data['message']);
	  				return;
	  			}
          // data['data']['role'][0]['slug'] == "employee"
          if ( true ) {
            
            // Redirect to new password
            if ( data['fromReset'] ) {
              this.navCtrl.push(NewPasswordPage, {
                email: this.loginForm.value['email'],
                password: this.loginForm.value['password'],
                userData: data['data'],
                bg: me.getBg()
              });

              return;
            }

            this.userProvider.create(data['data']).then(
              success => {
                this.navCtrl.push(WeeklyPage, {
                  fromLogin: true
                });
              }
            );
          } 

          // else {
          //   this.alerts.show('Warning', 'This app is for employees only, please use the Tapi website for admins and scheduler.');
          // }
	  			
	  		}, 
	  		err => {
	  			// this.alerts.hideLoader();
	  			this.helper.serverError();
	  		}
  		);
  	}
  }

  /**
   * Validates the form
   */
  validate() {
  	let formControl = this.loginForm.controls;

  	if ( !formControl['email'].valid ) {
  		this.alerts.show('Oops', 'Your email address format is not correct. Please try again.');
  		return false;
  	}

  	else if ( !formControl['password'].valid ) {
  		this.alerts.show('Oops', 'The password you\'ve used is invalid. Please try again.');
  		return false;
  	}

  	return true;
  }

  /**
   * Loads the Forgot Password page
   */
  presentForgotPassword() {
    this.navCtrl.push(ForgotPasswordPage, {
      bg: this.getBg()
    });
  }

  /**
   * Returns the generated bg
   * @returns {string} bg class
   */
  getBg() {
    return this.bgs[this.generatedNum];
  }

  /**
   * Opens TapiApp website
   */
  openWebsite() {
    this.refWindow = window.open('https://tapiapp.com/', '_system', 'location=yes');
  }

  /**
   * Opens TapiApp registration page
   */
  openRegistrationSite() {
    this.refWindow = window.open('https://tapiapp.com/pricing#register-panel', '_system', 'location=yes');
  }

}
