import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Models
import { UserModel } from '../models/user-model'

// Providers
import { Config } from './config';
import { Helper } from './helper';
import { Alerts } from './alerts';

// Loki Declaration
declare var require: any;
var loki = require('lokijs');

/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserProvider {

  user: any;

  headers: any;
  phpUrl: any;

  fetchedData: any;

  constructor(
    private store: Storage,
    private http: Http,
    private config: Config,
    public helper: Helper,
    private alerts: Alerts,
    private platform: Platform
  ) {

    // Create headers
    this.headers = new Headers()
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

    // Get php url from config
    this.phpUrl = this.config.getPhpUrl();
  }

  /**
   * Creates instance of User
   * @param {any} data User data
   * @returns {boolean} 
   */
  create(data: any) {

    if (data && Object.keys(data).length > 0) {
      console.log(data);
      this.user = new UserModel(data);
        return this.save().then(
          success => {
            return true;
          },
          err => {
            console.log(err);
            return false;
          }
        );
    }
  }

  /**
   * Loads the current User
   * @returns {any} UserModel instance
   */
  load() {
  	return this.store.get('User').then((data) => {
  		this.user = new UserModel(JSON.parse(data));
  	});
  }

  /**
   * Saves the current User to database
   * @returns {any} Promise callback
   */
  save() {
  	return new Promise((resolve, reject) => {

	  	if (typeof(this.user) === "undefined") {
	  		reject('User data not set');
	  	}

	  	var userData = JSON.stringify(this.user);
	  	this.store.set('User', userData).then((stat) => {
	  		resolve(true);
	  	});

	  });
  }

  /**
   * Removes the current User to database
   * @returns {any}
   */
  remove() {
    return new Promise((resolve, reject) => {

      this.store.remove('User').then((stat) => {
        resolve(true);
      });

    });
  }

  /**
   * Fetches the latest User details
   * @returns {any}
   */
  refresh() {
    var params = {};

    params['uuid'] = this.helper.getUUID();
    params['token'] = this.user.token.token;
    params = this.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'refresh-token', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
          if ( (data.status && data.status !== 200) || typeof(data.data) === "undefined") {
            reject(data);
          }
          resolve(this.create(data.data).then(stat => {
            this.load();
          }));

        },
        err => {
          this.alerts.hideLoader();
          reject(err);
        }
      );
    });
  }

  /**
   * Handles error
   * @param {any} error [Error text]
   */
  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
