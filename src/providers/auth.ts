import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Providers
import { Config } from './config';
import { Helper } from './helper';
import { Alerts } from './alerts';
import { UserProvider } from './user-provider';
import { TaskProvider } from './task-provider';
import { NotificationProvider } from './notification-provider';

/*
  Generated class for the Auth provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Auth {

  headers: any;
  phpUrl: any;

  constructor(
  	private config: Config,
  	private http: Http,
  	private storage: Storage,
  	public helper: Helper,
    public alerts: Alerts,
    public platform: Platform,
    public userProvider: UserProvider,
    public taskProvider: TaskProvider,
    public notificationProvider: NotificationProvider
  ) {

  	// Create headers
  	this.headers = new Headers()
  	this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

  	// Get php url from config
  	this.phpUrl = this.config.getPhpUrl();
  }

  /**
   * Logs-in the user
   * @param {any} credentials credentials object (with email and password)
   * @returns {Promise} returns user data otherwise error
   */
  login(credentials: any) {
  	credentials.uuid = this.helper.getUUID();
  	let creds = this.helper.serialize(credentials);

  	return new Promise((resolve, reject) => {
  		this.http.post(
			this.phpUrl + 'login', creds, { 
				headers: this.headers
			})
		.map(res => res.json())
		.catch(this.handleError)
		.subscribe(
			data => {
		   	resolve(data);
			},
			err => {
				reject(err);
			}
		);
  	});
  }

  /**
   * Logs-out the user
   * @returns {Promise} returns true if logged out properly, otherwise error
   */
  logout() {
    var me = this;
    var params = {};
    params['uuid'] = this.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;

    params = this.helper.serialize(params);

    this.alerts.showLoader();

    // Unregister push notification
    me.notificationProvider.unregister();

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'logout', params, { 
        headers: this.headers
      })
    .map(res => res.json())
    .catch(this.handleError)
    .subscribe(
      data => {
        this.alerts.hideLoader();

        // Clear collection
        this.taskProvider.tasks.clear();
        this.taskProvider.tasks_dates.clear();

        resolve(this.userProvider.remove().then(hasRemoved => {
          return true;
        }));
      },
      err => {
        this.alerts.hideLoader();
        reject(err);
      }
    );
    });
  }

  /**
   * Checks if the user is currently logged in
   * @returns {Boolean}
   */
  checkAuthorisation() {
  	return this.storage.get('User').then(
  		user => {
  			if ( !user ) {
	  			return false;
	  		}
	  		return true;
  		}
  	);
  }

  /**
   * Handles error
   * @param {any} error Error object
   */
  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
