import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import * as moment from 'moment';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Providers
import { Config } from './config';
import { Helper } from './helper';
import { Alerts } from './alerts';
import { UserProvider } from './user-provider';

// Loki Declaration
declare var require: any;
var loki = require('lokijs');


@Injectable()
export class EmployeeProvider {

  headers: any;
  phpUrl: any;

  fetchedData: any;

  departments: any;

  constructor(
  	private http: Http,
  	private config: Config,
  	private helper: Helper,
  	private alerts: Alerts,
  	private userProvider: UserProvider
  ) {

    // Create headers
  	this.headers = new Headers()
  	this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

  	// Get php url from config
  	this.phpUrl = this.config.getPhpUrl();
  }

  loadEmployees() {
  	return new Promise((resolve, reject) => {
  	  this.fetchEmployees().then(data => {
  	  	this.departments = data;
  	  	resolve(data);
	  },
	  err => {
        reject(err);
	  });
	});
  }

  filterEmployees(queryText: string) {

    if ( queryText.length > 0 ) {
    	queryText = queryText.toLowerCase().replace(/,|\.|-/g, ' ');
	    var queryWords = queryText.split(' ').filter(w => !!w.trim().length);

	    let employee_name;

	    this.departments.forEach((department: any) => {
	    	department.users.forEach((user:any) => {
	    		employee_name = user.name.toLowerCase();
	    		user.hidden = true;

	    		queryWords.forEach((query: any) => {
	    			if ( employee_name.indexOf(query) > -1 ) {
		    			user.hidden = false;
		    		}
	    		});
	    	});
	    });
    } else {
    	this.departments.forEach((department: any) => {
	    	department.users.forEach((user:any) => {
	    		user.hidden = false;
	    	});
	    });
    }
    
    return this.departments;
  }

  fetchEmployees() {
  	var params = {};

  	params['uuid'] = this.helper.getUUID();
  	params['token'] = this.userProvider.user.token.token;
  	params['company_id'] = this.userProvider.user.company_id;

  	params = this.helper.serialize(params);

  	return new Promise((resolve, reject) => {
  	  this.alerts.showLoader();
  	  this.http.post(
  	  	this.phpUrl + 'users/get', params, { 
		headers: this.headers
	  })
	  .map(res => res.json())
	  .catch(this.handleError)
	  .subscribe(
	  	data => {
	  	  this.alerts.hideLoader();

	  	  if (data.status && data.status !== 200) {
	  	   reject(data);
	  	  }

          resolve(data);
		},
		err => {
		  this.alerts.hideLoader();
		  reject(err);
		});
  	});
  }

  /**
   * Error handler
   * @param {any} error error object
   */
  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
