import { Injectable } from '@angular/core';
import { Device } from 'ionic-native';
import 'rxjs/add/operator/map';

// Providers
import { Alerts } from './alerts';

/*
  Generated class for the Helper provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Helper {

  constructor(
  	public alerts: Alerts
  ) {
  }

  /**
   * Gets device UUID
   * @returns {String} UUID
   */
  getUUID() {
  	if (typeof(Device.uuid) == "object") {
      var uuid;

  		// return this.generateRandomString(18);
      if (localStorage.getItem('uuid')) {
        return localStorage.getItem('uuid');
      } else {
        uuid = this.generateRandomString(26);
        localStorage.setItem('uuid', uuid);
        return uuid;
      }
  	}
  	return Device.uuid;
  }

  /**
   * Generates random string based on the provided length
   * @param {Number} length string length
   * @returns {String} random string
   */
  generateRandomString(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ ) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  /**
   * Serializes object for AJAX
   * @param {any} obj parameter object
   * @returns {String} serialized object
   */
  serialize(obj: any) {
  	var a = [];

  	for (var p in obj) {
  		a.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
  	}
    
	  return a.join("&");
  }

  /**
   * Handler for server error
   * @param {String} err error
   */
  serverError(err?: any) {
  	err = !err ? 'There was a problem in connecting to server. ' + "\n" + 'Please try again.' : err;
  	this.alerts.hideLoader();
  	this.alerts.show('Uh oh', err);

  	return;
  }

  /**
   * Converts Hex to RGB equivalent
   * @param {String} hex Hex string
   * @returns {any} converted RGB
   */
  hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
  }

  /**
   * Converts string to array
   * @param {String} val string
   * @returns {Array}
   */
  convert2Array(val) {
    return Array.from(val);
  }

  /**
   * Checks if a value exists in an array
   * @param {any} needle    value to find
   * @param {Array} haystack  array to find against
   * @param {Boolean} argStrict true if comparison is strict
   * @returns {Boolean} returns true if something is found, otherwise false
   */
  in_array(needle, haystack, argStrict?) {
    var key = '';
    var strict = !!argStrict;

    // we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] === ndl)
    // in just one for, in order to improve the performance
    // deciding wich type of comparation will do before walk array
    if (strict) {
      for (key in haystack) {
        if (haystack[key] === needle) {
          return true;
        }
      }
    } else {
      for (key in haystack) {
        if (haystack[key] == needle) { // eslint-disable-line eqeqeq
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Error handler
   * @param {any} err error object
   */
  handleError(err) {
    this.alerts.hideLoader();
    var status = err.status ? err.status : 400;
    var message = err.message ? err.message : "Something went wrong.";

    console.log(err);

    if ( status === 401 ) {
      this.alerts.show('Uh oh', 'Invalid token, please re-login');
      // this.logout.redirectLogout();
      return;
    }

    this.alerts.show('Uh oh', message);

  }

  /**
   * Generates a random integer number between 2 given numbers
   * @param {number} lower lowest number to be generated
   * @param {number} upper highest number to be generated
   */
  getRandomNumber(lower: number, upper: number) {
    return Math.floor(lower + (Math.random() * (upper - lower + 1)));
  }



}
