import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Providers
import { Config } from './config';
import { Helper } from './helper';
import { Alerts } from './alerts';

/*
  Generated class for the PasswordProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PasswordProvider {

  headers: any;
  phpUrl: any;

  constructor(
  	public http: Http,
  	private config: Config,
  	private helper: Helper,
  	private alerts: Alerts,
  ) {

  	// Create headers
  	this.headers = new Headers()
  	this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

  	// Get php url from config
  	this.phpUrl = this.config.getPhpUrl();
  }

  /**
   * Sends forgot password request to server
   * @param {any} formdata form data
   * @returns {Promise} server feedback
   */
  requestForgotPW(formdata) {
  	var params = {};
  	params['email'] = formdata.email;
  	params = this.helper.serialize(params);

  	return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'reset-email', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
          if ( data.status && data.status !== 200) {
            reject(data);
          }
          resolve(data);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  /**
   * Requests password reset to server
   * @param {any} formdata form data
   * @returns {Promise} server feedback
   */
  updatePassword(formdata) {
    var params = {};
    params['email'] = formdata.email;
    params['password'] = formdata.password;
    params['password_confirm'] = formdata.password_confirm;
    params['temp_password'] = formdata.temp_password;

    params = this.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'reset-password', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
          if ( data.status && data.status !== 200) {
            reject(data);
          }
          resolve(data);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  /**
   * Error handler
   * @param {any} error error object
   */
  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
