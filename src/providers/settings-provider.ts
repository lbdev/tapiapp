import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Providers
import { Config } from './config';
import { UserProvider } from './user-provider';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SettingsProvider {

  headers: any;
  phpUrl: any;

  constructor(
  	public http: Http,
    public config: Config,
    public userProvider: UserProvider,
  ) {

  	// Create headers
    this.headers = new Headers()
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

    // Get php url from config
    this.phpUrl = this.config.getPhpUrl();
  }

  /**
   * Updates user name
   * @param {String} name updated name
   */
  updateName(name:String) {
  	let params = {}

    params['user_id'] = this.userProvider.user.id;
    params['name'] = name;
    params['uuid'] = this.userProvider.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;
    params = this.userProvider.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'users/update/name', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
           resolve(data);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  /**
   * Updates password
   * @param {String} currentPW current password
   * @param {String} newPW     new password
   */
  updatePassword(currentPW: String, newPW: String) {
  	let params = {}

    params['user_id'] = this.userProvider.user.id;
    params['current_password'] = currentPW;
    params['new_password'] = newPW;
    params['uuid'] = this.userProvider.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;
    params = this.userProvider.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'users/update/password', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
           resolve(data);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  /**
   * Handles error
   * @param {any} error Error object
   */
  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
