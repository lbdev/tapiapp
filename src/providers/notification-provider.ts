import { Injectable } from '@angular/core';
import { Push } from 'ionic-native';
import { AlertController, Events, Platform } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Providers
import { Config } from './config';
import { UserProvider } from './user-provider';

/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NotificationProvider {

  headers: any;
  phpUrl: any;

  push: any;
  token: any;

  constructor(
    public config: Config,
    public userProvider: UserProvider,
    private http: Http,
    private platform: Platform,
    private alertCtr: AlertController,
    private events: Events
  ) {
    // Create headers
    this.headers = new Headers()
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

    // Get php url from config
    this.phpUrl = this.config.getPhpUrl();
  }

  /**
   * Initialize Push Notification
   */
  init() {
    console.log(this);
    console.log("Initting push");

    this.push = Push.init({
      android: {
        senderID: '422903876297',
        icon: 'ic_stat_tapi',
        iconColor: 'grey'
      },
      ios: {
        alert: true,
        badge: true,
        sound: true
      },
    });

    // Reset badge count
    if ( typeof(window['cordova']) !== "undefined" ) {
      this.push.setApplicationIconBadgeNumber(function() {
        console.log('Badge icon number cleared');
      },
      function() {
        console.log('Clearing badge number error');
      }, 0);
    }
      

    if ( !this.push['error'] ) {
      this.addEvents();
    }
  }

  /**
   * Add event listeners for Push Notification
   */
  addEvents() {
    var me = this;

    // Triggers when PN is registered
    this.push.on('registration', function(data) {
      console.log(data.registrationId);
      me.token = data.registrationId;

      me.events.publish('pushnotification:registered');
      me.storeToken().then(response => {
        localStorage.setItem('pushToken', data.registrationId);

        if (response['status'] && response['status'] !== 200) {
          return;
        }
      });
    });

    // Triggers when received PN, either from coldstart or foreground
    this.push.on('notification', function(data) {
      console.log(data);

      let alert = me.alertCtr.create({
        title: data.title ? data.title : "Notification",
        subTitle: data.message,
        buttons: [{
          text: 'OK',
          handler: () => {

            let intent = me.getPayloadIntent(data);
            let isForeground = data.additionalData.foreground;
            let isColdstart = data.additionalData.coldstart;

            let isRelatedToTask = intent && ( 
              intent === "task-added"   || 
              intent === "task-deleted" || 
              intent === "task-updated"
            );

            if ( isRelatedToTask && (isForeground || !isColdstart) ) {
              me.events.publish('pushnotification:received');
            }
            
          }
        }]
      });

      alert.present();
    });

    this.push.on('error', function(e) {
      console.log(e.message);
    });
  }

  /**
   * Unregisters Push Notification
   * @returns {Promise} if PN successfully unregistered
   */
  unregister() {
    var me = this;

    me.destroyToken().then(response => {
      return new Promise((resolve, reject) => {
        if ( !me.push['error'] ) {
          me.push.unregister(
            function() {
              console.log('Push Notification unsubscribed');
              me.events.publish('pushnotification:unregistered');
              localStorage.removeItem('pushToken');

              resolve(true);
            },
            function(e) {
              console.log('An error has occured: ' + e);
              resolve(false);
            }
          );
        }
      });
    });
  }

  /**
   * Checks permission of Push Notification
   */
  hasPermission() {
  	return this.push.hasPermission();
  }

  storeToken() {
    let params = {}

    params['user_id'] = this.userProvider.user.id;
    params['uuid'] = this.userProvider.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;
    params['push_token'] = this.token ? this.token : "";
    params['os'] = this.platform.is("ios") ? "ios" : "android";
    params = this.userProvider.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'device-tokens/store', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
           resolve(data);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  destroyToken() {
    let params = {}

    params['user_id'] = this.userProvider.user.id;
    params['uuid'] = this.userProvider.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;
    params['push_token'] = this.token ? this.token : "";
    params['os'] = this.platform.is("ios") ? "ios" : "android";
    params = this.userProvider.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'device-tokens/destroy', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
           resolve(data);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  /**
   * Extracts intent data from push notification payload
   * @param {any} payload push notification payload
   */
  getPayloadIntent(payload: any) {

    let platform = this.platform.is("ios") ? "ios" : "android";
    let intent = "";

    if ( platform === "ios" ) {
      intent = payload.additionalData.intent ? payload.additionalData.intent : "";
    } 

    else if ( platform === "android" ) {
      if ( payload.additionalData.custom ) {
        intent = payload.additionalData.custom.intent ? payload.additionalData.custom.intent : "";
      }
    }

    return intent ? intent : "";
  }

  /**
   * Handles error
   * @param {any} error Error object
   */
  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
