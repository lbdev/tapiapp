import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import * as moment from 'moment';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Providers
import { Config } from './config';
import { Helper } from './helper';
import { Alerts } from './alerts';
import { UserProvider } from './user-provider';


// Loki Declaration
declare var require: any;
var loki = require('lokijs');

@Injectable()
export class TaskProvider {

  headers: any;
  phpUrl: any;

  db: any;
  tasks: any;
  tasks_dates: any;

  fetchedData: any;

  lastSync: any;

  constructor(
  	private http: Http,
  	private config: Config,
  	private helper: Helper,
  	private alerts: Alerts,
  	private userProvider: UserProvider
  ) {

  	// Setup db
  	this.db = new loki('Tapi');
  	this.tasks = this.db.addCollection('tasks');
  	this.tasks_dates = this.db.addCollection('tasks_dates');

  	// Create headers
  	this.headers = new Headers()
  	this.headers.append('Content-Type', 'application/x-www-form-urlencoded');

  	// Get php url from config
  	this.phpUrl = this.config.getPhpUrl();
  }

  /**
   * Loads daily task based on the provided date
   * @param {any} date intent date
   * @returns {Promise} task data
   */
  getDailyTask(date?: any) {
  	var intent_date = date.format('DD-MM-YYYY');
  	var is_loaded = this.tasks_dates.find({ 'day': intent_date });

  	return new Promise((resolve, reject) => {
  		if ( is_loaded.length > 0) {
        this.lastSync = moment();

  			var tasks_loaded = this.tasks.find({ 'date_assigned': intent_date });
        var tasks_day = this.tasks_dates.find({ 'day': intent_date });

        var data = {
          tasks: tasks_loaded,
          leave: {
            'leave_hours': tasks_day[0]['leave_hours'],
            'leave_type': tasks_day[0]['leave_type'],
            'on_leave': tasks_day[0]['on_leave']
          }
        };

	  		resolve(data);
	  	} else {

	  		resolve(this.loadWeeklyTasks(date).then(success => {
	  			return this.getDailyTask(date);
	  		}));
	  	}
  	});
  	
  }

  /**
   * Loads weekly tasks based on the provided date
   * @param {any} date intent date
   */
  loadWeeklyTasks(date?: any) {

  	var intent_date;

  	return this.fetchWeeklyTasks(date).then(
  		data => {
  			this.fetchedData = data;
  			this.fetchedData.forEach(item => {
  				intent_date = moment(item.day, 'DD-MM-YYYY').format('DD-MM-YYYY');
  				var found_dates = this.tasks_dates.find({ 'day': intent_date });

  				if ( found_dates > 0 ) {
  					found_dates.day = item.day;
  					found_dates.on_leave = item.on_leave;
            found_dates.leave_hours = item.leave_hours;
            found_dates.leave_type = item.leave_type;
  					this.tasks_dates.update(found_dates);
  				} else {
  					this.tasks_dates.insert({
  						'day': item.day,
  						'on_leave': item.on_leave,
              'leave_hours': item.leave_hours,
              'leave_type': item.leave_type
  					});
  				}

  				item['tasks'].forEach(taskItem => {
  					var found = this.tasks.find({ 'id' : taskItem['id']  });

  					if ( found.length > 0 ) {
  						this.tasks.remove(found);
  						this.tasks.insert(taskItem);
  					} else {
  						this.tasks.insert(taskItem);
  					}
  				});
  			});
  		}
  	);
  }

  /**
   * Refreshes the daily task based on the intent date (for pull refresh)
   * @param {any} date intent date
   */
  refreshDailyTasks(date?: any) {
    return this.fetchDailyTasks(date).then(data => {
      this.fetchedData = data;

      var found = this.tasks.find({ 'date_assigned' : date.format('DD-MM-YYYY')  });
      if ( found.length > 0) {
        this.tasks.remove(found);
      }

      if(this.fetchedData.tasks && this.fetchedData.tasks.length > 0) {

        this.fetchedData.tasks.forEach(taskItem => {
          this.tasks.insert(taskItem);
        });

        // Update on leave
        var found_dates = this.tasks_dates.find({ 'day': date.format('DD-MM-YYYY') });
        if (found_dates) {
          this.tasks_dates.remove(found_dates);
          this.tasks_dates.insert({
            'day': date.format('DD-MM-YYYY'),
            'on_leave': this.fetchedData.on_leave,
            'leave_hours': this.fetchedData.leave_hours,
            'leave_type': this.fetchedData.leave_type
          });
        }
      }

      return this.getDailyTask(date);

    });
  }

  /**
   * Toggles the task status
   * @param {any} taskId task ID
   * @param {any} status task status
   * @returns {Promise} server feedback
   */
  toggleTaskStatus(taskId:any, status) {
    var url = status === "" ? "incomplete" : "complete";
    var params = {};
    params['uuid'] = this.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;
    params['user_task_id'] = taskId;

    params = this.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'tasks/' + url, params, { 
        headers: this.headers
      })
    .map(res => res.json())
    .catch(this.handleError)
    .subscribe(
      data => {

        if (data.status && data.status !== 200) {
          reject(data);
        }
        
        resolve(data);
        
      },
      err => {
        reject(err);
      }
    );
    });
  }

  /**
   * Fetches Weekly task on the server
   * @param {any} date intent date
   * @returns {Promise} server feedback
   */
  fetchWeeklyTasks(date?: any) {
  	var params = {};

  	date = date ? date.format('DD-MM-YYYY') : moment().format('DD-MM-YYYY');
  	params['date'] = date;
  	params['uuid'] = this.helper.getUUID();
  	params['token'] = this.userProvider.user.token.token;
  	params['user_id'] = this.userProvider.user.id;
  	params['company_id'] = this.userProvider.user.company_id;

  	params = this.helper.serialize(params);

  	return new Promise((resolve, reject) => {
  		this.alerts.showLoader();
  		this.http.post(
			this.phpUrl + 'tasks/get-user-week', params, { 
				headers: this.headers
			})
		.map(res => res.json())
		.catch(this.handleError)
		.subscribe(
			data => {
				this.alerts.hideLoader();

        if (data.status && data.status !== 200) {
          reject(data);
        }

        resolve(data);
		    
			},
			err => {
				this.alerts.hideLoader();
				reject(err);
			}
		);
  	});
  }

  /**
   * Fetches daily task on the server
   * @param {any} date intent date
   * @returns {Promise} server feedback
   */
  fetchDailyTasks(date?: any) {
    var params = {};

    date = date ? date.format('DD-MM-YYYY') : moment().format('DD-MM-YYYY');
    params['date'] = date;
    params['uuid'] = this.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;
    params['user_id'] = this.userProvider.user.id;
    params['company_id'] = this.userProvider.user.company_id;

    params = this.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'tasks/get-user-day', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {

          if ( data.status && data.status !== 200 )  {
            reject(data);
          }
          
          resolve(data);

        },
        err => {
          this.alerts.hideLoader();
          reject(err);
        }
      );
    });
  }

  /**
   * Fetches comment based on the provided date
   * @param {any} date intent date
   * @returns {Promise} server feedback
   */
  fetchComment(date?: any) {
    var params = {};

    date = date ? date.format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');
    params['date'] = date;
    params['uuid'] = this.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;
    params['user_id'] = this.userProvider.user.id;

    params = this.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'comments/get', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
          if ( data.status && data.status !== 200) {
            reject(data);
          }
          
          resolve(data);
        },
        err => {
          this.alerts.hideLoader();
          reject(err);
        }
      );
    });
  }

  /**
   * Submits comment to server
   * @param {any}       date       intent date
   * @param {String}    comment    user comment
   * @returns {Promise} server feedback
   */
  addComment(date?: any, comment?: any) {
    var params = {};

    date = date ? date.format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');
    params['date'] = date;
    params['uuid'] = this.helper.getUUID();
    params['token'] = this.userProvider.user.token.token;
    params['user_id'] = this.userProvider.user.id;
    params['comment'] = !comment ? "" : comment;

    params = this.helper.serialize(params);

    return new Promise((resolve, reject) => {
      this.http.post(
      this.phpUrl + 'comments/store', params, { 
        headers: this.headers
      })
      .map(res => res.json())
      .catch(this.handleError)
      .subscribe(
        data => {
          if ( data.status && data.status !== 200) {
            reject(data);
          }
          resolve(data);
        },
        err => {
          this.alerts.hideLoader();
          reject(err);
        }
      );
    });
  }

  /**
   * Error handler
   * @param {any} error error object
   */
  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
