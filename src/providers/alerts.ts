import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Dialogs } from 'ionic-native';

/*
  Generated class for the Alerts provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Alerts {

  public loader: any;
  public window: any;

  constructor(
  	private alertCtrl: AlertController,
  	private loadingCtrl: LoadingController
  	) {
  }

  /**
   * Shows alert box
   * @param {string} title    = '' Alert title
   * @param {string} subtitle = '' Alert message
   */
  show(title = '', subtitle = '') {

    if ( typeof(window['cordova']) !== "undefined" ) {
      Dialogs.alert(subtitle, title, "OK");
      return;
    }

  	let alert = this.alertCtrl.create({
  		title: title,
  		subTitle: subtitle,
  		buttons: [
  			{
  				text: 'OK'
  			}
      ]
  	});

  	alert.present();
  }

  /**
   * Shows loading indicator
   * 
   */
  showLoader(): void {

  	this.loader = this.loadingCtrl.create();
  	this.loader.present();
    
  }

  /**
   * Hides loading indicator
   */
  hideLoader(): void {
    
    if ( this.loader ) {
      this.loader.dismiss();
    }
  	
  }



}
