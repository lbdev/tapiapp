import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';

/*
  Generated class for the Config provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Config {

	version = {
        string: '1.0.2',
        key: 'tapi_version'
    };

    baseUrl = '';

    phpUrl = '';

    urls = {
        local: 'http://192.168.128.136/TapiWebsite/tapiv2/mobile-api/',
        testing: 'https://tapiapp.com/app/mobile-api/',
        production: 'https://tapiapp.com/app/mobile-api/'
    };

    assetUrls = {
        local: 'https://tapiapp.com/app/public/',
        testing: 'https://tapiapp.com/app/public/',
        production: 'https://tapiapp.com/app/public/'
    };

    storeUrls = {
        Android: 'https://play.google.com/store/apps/details?id=com.leadingbrands.Tapi',
        iOS: 'https://itunes.apple.com/us/app/tapi-app/id1129149637?ls=1&mt=8',
        shortLinks: {
            Android: 'https://goo.gl/5W0ETc',
            iOS: 'https://goo.gl/5QSqnJ'
        }
    };

    phpFolders = {
        local: 'v2/',
        testing: 'v2/',
        production: 'v2/'
    };

    assetsFolder = {
        logo: 'uploads/logo/'
    };
    
    rating = {

        appIds: {
            ios: '1031481153',
            android: 'com.leadingbrands.Tapi'
        },

        counterKey: 'tapi_rate_counter'
    };

    config = {

        appName: 'TapiApp',

        env: 'production'
    };

  constructor(
  	private storage: Storage,
  	private platform: Platform
  ) {
  	this.baseUrl = this.getBaseUrl();
  	this.setPhpUrl(this.getPhpUrl());
  }

  /**
   * Gets the base URL
   * @returns {String} URL
   */
  getBaseUrl() {
  	let urls = this.urls;
  	let origin = window.location.origin;

    for (var key in urls) {

        if (urls.hasOwnProperty(key)) {
            var url = urls[key];

            if (url.indexOf(origin) === 0) {

                this.config.env;

                return url;
            }
        }
    }
    return urls.production;
  }

  /**
   * Sets the base URL
   * @param {any} url URL
   */
  setBaseUrl(url: any) {
  	this.baseUrl = url;
  }

  /**
   * Gets the API PHP URL
   * @returns {String} URL
   */
  getPhpUrl() {
  	let baseUrl = this.baseUrl || this.getBaseUrl();
  	return baseUrl + this.phpFolders[this.config.env];
  }

  /**
   * Sets the phpUrl object
   * @param {any} url URL
   */
  setPhpUrl(url: any) {
	    this.phpUrl = url;
  }

  /**
   * Saves the app version on storage
   * @param {any} version version string
   */
  saveAppVersion(version: any) {
  	this.storage.set(this.version.key, this.version.string);
  }

  /**
   * Gets the app store URL based on device OS
   * @returns {String} Store URL
   */
  getStoreURL() {
    let os = this.platform.is('ios') ? 'iOS' : 'Android';
    return this.storeUrls[os];
  }

  /**
   * Gets the shortened app store URL
   * @returns {String} Store URL
   */
  getStoreURLShort() {
    let os = this.platform.is('ios') ? 'iOS' : 'Android';
	  return this.storeUrls.shortLinks[os];
  }

  /**
   * Gets the logo folder URL
   * @returns {String} logo folder URL
   */
  getLogoFolderURL() {
    return this.assetUrls.production + this.assetsFolder.logo;
  }

}
