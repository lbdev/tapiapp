import { Injectable } from '@angular/core';
import { MenuController, App } from 'ionic-angular';
import 'rxjs/add/operator/map'

// Pages
import { LoginPage } from '../pages/login/login';

// Providers
import { Auth } from './auth';

/*
  Generated class for the Logout provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Logout {

  nav: any;

  constructor(
  	private authProvider: Auth,
  	private menuCtrl: MenuController,
  	private app: App
  ) {

  	this.nav = app.getActiveNav();
  }

  /**
   * Logs-out the user redirects to homepage
   */
  logout() {
  	this.menuCtrl.close('weeklyMenu');
    this.authProvider.logout().then(hasLoggedOut => {
      this.nav.push(LoginPage).then(() => {
        // first we find the index of the current view controller:
        const index = this.nav.getActive().index;
        // then we remove it from the navigation stack
        this.nav.remove(0, index);
        this.menuCtrl.swipeEnable(false, 'weeklyMenu');
      });   
    });
  }

}
