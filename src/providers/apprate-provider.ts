import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AppRate } from 'ionic-native';

import 'rxjs/add/operator/map';

/*
  Generated class for the ApprateProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ApprateProvider {

  isShown = false;

  constructor(public http: Http) {

  	let me = this;

  	AppRate.preferences = {
	  openStoreInApp: false,
	  displayAppName: 'TapiApp',
	  usesUntilPrompt: 10,
	  promptAgainForEachNewVersion: false,
	  storeAppURL: {
	    ios: '1129149637',
	    android: 'market://details?id=com.leadingbrands.Tapi'
	  },
	  customLocale: {
	    title: "Rate TapiApp",
	    message: "Please rate me to make TapiApp the best app.",
	    cancelButtonLabel: "No thanks",
	    laterButtonLabel: "Maybe later",
	    rateButtonLabel: "OK rate now"
	  },
	  callbacks: {
	  	onButtonClicked: function() {
	  		me.isShown = false;
	  	},

	  	onRateDialogShow: function() {
	  		me.isShown = true;
	  	}
	  }
	};
  }

  prompt() {
  	if ( !this.isShown )
  		AppRate.promptForRating(false);
  }

}
