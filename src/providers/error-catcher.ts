import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

// Providers
import { Alerts } from './alerts';
import { Logout } from './logout';

/*
  Generated class for the ErrorHandler provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ErrorCatcher {

  constructor(
  	public alerts: Alerts,
  	public logout: Logout
  	) {
  }

  handleError(err) {
  	this.alerts.hideLoader();
    var status = err.status ? err.status : 400;
    var message = err.message ? err.message : "Something went wrong.";

    console.log(err);

    if ( status === 401 ) {
      this.alerts.show('Uh oh', 'Invalid token, please re-login.');
      this.logout.logout();
      return;
    }

    this.alerts.show('Warning', message);
  }

}
