import { Component, ViewChild } from '@angular/core';

import { Platform, Nav, MenuController, ModalController } from 'ionic-angular';

import { StatusBar, Splashscreen } from 'ionic-native';

// Pages
import { LoginPage } from '../pages/login/login';
import { WeeklyPage } from '../pages/weekly/weekly';
import { SettingsPage } from '../pages/settings/settings';
import { EmployeeListPage } from '../pages/employee-list/employee-list';


// Providers
import { Auth } from '../providers/auth';
import { UserProvider } from '../providers/user-provider';
import { NotificationProvider } from '../providers/notification-provider';

// Interfaces
export interface PageInterface {
  title: string;
  component?: any;
  icon: string;
  index?: number;
  logsOut?: boolean;
}

@Component({
  templateUrl: 'app.html',
  providers: []
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage: any;
  pages: Array<{title: string, component: any}>;

  // menu items
  schedulePages: PageInterface[] = [
    { title: 'My Tasks', component: WeeklyPage, icon: 'calendar', index: 1 },
    { title: 'Employees', component: EmployeeListPage, icon: 'contacts', index: 2 }
  ];

  accountPages: PageInterface[] = [
    { title: 'Settings', component: SettingsPage, icon: 'settings', index: 3 },
    { title: 'Logout', icon: 'log-out', logsOut: true}
  ];

  constructor(
    public platform: Platform,
    public authProvider: Auth,
    public userProvider: UserProvider,
    public notificationProvider: NotificationProvider,
    private menuCtrl: MenuController,
    private modalCtrl: ModalController
  ) {
    this.initializeApp();

    this.authProvider.checkAuthorisation().then(
      status => {
         if (status) {
           this.userProvider.load().then(
             response => {
               this.nav.setRoot(WeeklyPage);
               return;
             },

             err => {
                this.nav.setRoot(LoginPage);
             }
           );
         }
         
         this.rootPage = LoginPage;
      }
    );
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      // Set status bar style
      StatusBar.overlaysWebView(false); 
      StatusBar.backgroundColorByHexString('#000000');
      StatusBar.styleLightContent();

      // Hide splashscreen
      setTimeout(() => {
        Splashscreen.hide();
      }, 1000);
      
    });
  }

  /**
   * Logs out the user
   */
  logout() {
    this.menuCtrl.close('weeklyMenu');
    this.authProvider.logout().then(hasLoggedOut => {
      this.nav.push(LoginPage).then(() => {
        // first we find the index of the current view controller:
        const index = this.nav.getActive().index;
        // then we remove it from the navigation stack
        this.nav.remove(0, index);
        this.menuCtrl.swipeEnable(false, 'weeklyMenu');
      });   
    });
  }

  /**
   * Call to navigate to a page from menu
   * @param {PageInterface} page intent page
   */
  goToPage(page: PageInterface) {
    if ( page.index && !page.logsOut ) {
      this.menuCtrl.close('weeklyMenu');
      this.nav.setRoot(page.component, {

      });
    } else if ( page.logsOut ) {
      this.logout();
    } else {
      this.menuCtrl.close('weeklyMenu');
      this.nav.push(page.component, {
        
      });
    }
  }
}
