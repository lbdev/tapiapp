import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { MyApp } from './app.component';

// Pages
import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { NewPasswordPage } from '../pages/new-password/new-password';
import { WeeklyPage } from '../pages/weekly/weekly';
import { PopoverPage } from '../pages/weekly/popover';
import { TaskDetailsPage } from '../pages/task-details/task-details';
import { CommentPage } from '../pages/comment/comment';
import { SettingsPage } from '../pages/settings/settings';
import { EmployeeListPage } from '../pages/employee-list/employee-list';
import { EmployeeWeeklyPage } from '../pages/employee-weekly/employee-weekly';

// Providers
import { Config } from '../providers/config';
import { Helper } from '../providers/helper';
import { ErrorCatcher } from '../providers/error-catcher';
import { Alerts } from '../providers/alerts';
import { Auth } from '../providers/auth';
import { Logout } from '../providers/logout';
import { PasswordProvider } from '../providers/password-provider';
import { UserProvider } from '../providers/user-provider';
import { EmployeeProvider } from '../providers/employee-provider';
import { TaskProvider } from '../providers/task-provider';
import { EmployeeTaskProvider } from '../providers/employee-task-provider';
import { NotificationProvider } from '../providers/notification-provider';
import { SettingsProvider } from '../providers/settings-provider';
import { ApprateProvider } from '../providers/apprate-provider';

// Components

@NgModule({
  declarations: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    ListPage,
    LoginPage,
    ForgotPasswordPage,
    NewPasswordPage,
    WeeklyPage,
    PopoverPage,
    TaskDetailsPage,
    CommentPage,
    SettingsPage,
    EmployeeListPage,
    EmployeeWeeklyPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    ListPage,
    LoginPage,
    ForgotPasswordPage,
    NewPasswordPage,
    WeeklyPage,
    PopoverPage,
    TaskDetailsPage,
    CommentPage,
    SettingsPage,
    EmployeeListPage,
    EmployeeWeeklyPage
  ],
  providers: [
    {
        provide: ErrorHandler, 
        useClass: IonicErrorHandler
    },

    // Providers
    Storage,
    Config,
    Helper,
    ErrorCatcher,
    Alerts,
    Auth,
    Logout,
    PasswordProvider,
    UserProvider,
    EmployeeProvider,
    TaskProvider,
    EmployeeTaskProvider,
    NotificationProvider,
    SettingsProvider,
    ApprateProvider
  ]
})
export class AppModule {}
